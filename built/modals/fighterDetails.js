import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
export function showFighterDetailsModal(fighter) {
    const title = 'Fighter info';
    const bodyElement = createFighterDetails(fighter);
    showModal({ title, bodyElement });
}
function createFighterDetails(fighter) {
    const { name, health, attack, defense, source } = fighter;
    const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
    const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
    const attackElement = createElement({ tagName: 'span', className: 'fighter-attack' });
    const healthElement = createElement({ tagName: 'span', className: 'fighter-health' });
    const defenseElement = createElement({ tagName: 'span', className: 'fighter-defense' });
    const imageElement = createElement({ tagName: 'img', className: 'fighter-image' });
    imageElement.src = source;
    nameElement.innerText = `\n\rName: ${name}\n\r`;
    healthElement.innerText = `Health: ${health}\n\r`;
    attackElement.innerText = `Attack: ${attack}\n\r`;
    defenseElement.innerText = `Defense: ${defense}\n\r`;
    fighterDetails.append(imageElement, nameElement, healthElement, attackElement, defenseElement);
    return fighterDetails;
}
