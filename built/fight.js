export function fight(firstFighter, secondFighter) {
    let { health: firstFighterHealth } = firstFighter;
    let { health: secondFighterHealth } = secondFighter;
    while (firstFighterHealth >= 0 && secondFighterHealth >= 0) {
        firstFighterHealth -= getDamage(secondFighter, firstFighter);
        secondFighterHealth -= getDamage(firstFighter, secondFighter);
    }
    const winner = firstFighterHealth > 0 ? firstFighter : secondFighter;
    return winner;
}
export function getDamage(attacker, enemy) {
    const hit = getHitPower(attacker);
    const block = getBlockPower(enemy);
    let damage = hit - block;
    if (damage < 0)
        damage = 0;
    return damage;
}
export function getHitPower(fighter) {
    const { attack } = fighter;
    const criticalHitChance = Math.random() + 1;
    const hitPower = attack * criticalHitChance;
    return hitPower;
}
export function getBlockPower(fighter) {
    const { defense } = fighter;
    const dodgeChance = Math.random() + 1;
    const blockPower = defense * dodgeChance;
    return blockPower;
}
