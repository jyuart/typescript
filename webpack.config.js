var path = require('path');

module.exports = {
    // Change to your "entry-point".
    entry: './index.ts',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js',
        publicPath: '/dist/'
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.json']
    },
    module: {
        rules: [{
            // Include ts, tsx, js, and jsx files.
            test: /\.(ts|js)x?$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
            options: {
              configFile: "./babel.config.js",
              cacheDirectory: true
            }
        },
        {
          test: /\.css$/,
          use: ["style-loader", "css-loader"]
        }
      ],
    },
    mode: 'development',
    devServer: {
      host: '0.0.0.0',
      inline: true
    },
    devtool: "source-map"
}