import { callApi } from '../helpers/apiHelper';
import { Fighter } from '../fighterModel';

export async function getFighters() {
  try {
    const endpoint = 'fighters.json';
    const apiResult = await callApi(endpoint, 'GET');
    
    var result = <Fighter[]> apiResult;
    return result;
  } catch (error) {
    throw error;
  }
}

export async function getFighterDetails(id: string) {
  try{
    const endpoint = `details/fighter/${id}.json`;
    const apiResult = await callApi(endpoint, 'GET');

    var result = <Fighter> apiResult;
    return result;
  } catch (error) {
    throw error;
  }
}

