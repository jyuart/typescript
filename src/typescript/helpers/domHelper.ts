export function createElement({ tagName, className, attributes = {} } : creatingElement) {
  const element = document.createElement(tagName);
  
  if (className) {
    element.classList.add(className);
  }

  if (attributes)
    Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));

  return element;
}

interface creatingElement{
  tagName: string,
  className?: string,
  attributes?: { [index:string] : string }
}